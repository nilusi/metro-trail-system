package com.metro.rail.system.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="fare")
public class Fare {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long fareId;
	private String origin;
	private String destination;
	private double fareAmount;
	
	public Long getFareId() {
		return fareId;
	}
	public void setFareId(Long fareId) {
		this.fareId = fareId;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public double getFareAmount() {
		return fareAmount;
	}
	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}
	
}
