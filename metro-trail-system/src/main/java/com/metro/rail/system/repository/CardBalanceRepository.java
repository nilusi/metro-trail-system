package com.metro.rail.system.repository;

import org.springframework.data.repository.CrudRepository;

import com.metro.rail.system.model.CardBalance;

public interface CardBalanceRepository extends CrudRepository<CardBalance, Long> {

	
}
