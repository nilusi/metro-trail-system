package com.metro.rail.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.metro.rail.system.model.Trip;

@Service
public class ProducerServiceImpl {
    private static final Logger logger = LoggerFactory.getLogger(ProducerServiceImpl.class);
    private static final String TOPIC = "trip";

    @Autowired
    KafkaTemplate<String, Trip> kafkaTemplate;

    public void sendMessage(Trip trip){
        logger.info("Received Message:  "+trip);
        this.kafkaTemplate.send(TOPIC,trip);
    }
}
