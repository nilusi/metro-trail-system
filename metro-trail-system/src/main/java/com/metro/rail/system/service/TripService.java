package com.metro.rail.system.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.metro.rail.system.model.Fare;
import com.metro.rail.system.model.Trip;
import com.metro.rail.system.repository.CardBalanceRepository;
import com.metro.rail.system.repository.FareRepository;
import com.metro.rail.system.repository.TripRepository;
import com.metro.rail.system.response.CommonResponse;

@Component
public class TripService {

	@Autowired
	CardBalanceRepository cardBalanceRepo;
	
	@Value("${min.Amt.travel}")
	private double minAmount;
	
	@Autowired
	private FareRepository fareRepository;
	
	@Autowired
	private TripRepository tripRepository;
	
	@Autowired
	ProducerServiceImpl producerServiceImpl;
	
	
	public CommonResponse checkout(Long cardId, String origin, String destination) {

		CommonResponse response = new CommonResponse();
		
		//Get the fare amount and deduct it
		Fare fare = fareRepository.findByOriginAndDestination(origin, destination);
		
		if(null != fare){
			Trip trip = new Trip();
			trip.setFromZone(origin);
			trip.setToZone(destination);
			trip.setFareAmout(fare.getFareAmount());
			trip.setStartDate(new Date()); //consider about getting the strt time
			trip.setEndDate(new Date());
			trip.setCardId(cardId);
			tripRepository.save(trip);
			
			producerServiceImpl.sendMessage(trip);
		}
		
		response.setReturnMessage("Happy Travel");
		response.setStatusCode(HttpStatus.OK.value());
		return response;
	}
	
}
