package com.metro.rail.system.exception;

import com.metro.rail.system.util.ErrorCode;

public class RecordNotFoundException extends RuntimeException {

	 private static final long serialVersionUID = 1L;
	    private final String errorCode;

	    public String getErrorCode() {
			return errorCode;
		}

		public RecordNotFoundException(String message) {
	        super(message);
	        this.errorCode = ErrorCode.UNKNOWN_ERROR;
	    }

	    public RecordNotFoundException(String message, String errorCode) {
	        super(message);
	        this.errorCode = errorCode;
	    }

	    public RecordNotFoundException(String message, Throwable cause, String errorCode) {
	        super(message, cause);
	        this.errorCode = errorCode;
	    }
}
