package com.metro.rail.system.response;


public class CommonResponse {

	private int statusCode;
	private String returnMessage;
	
	public CommonResponse(int statusCode, String message) {
		this.statusCode=statusCode;
		this.returnMessage=message;
	}

	public CommonResponse() {
		// TODO Auto-generated constructor stub
	}

	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
