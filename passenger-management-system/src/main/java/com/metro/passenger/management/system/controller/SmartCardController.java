package com.metro.passenger.management.system.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metro.passenger.management.system.reponse.CommonResponse;
import com.metro.passenger.management.system.service.SmartCardService;

@RestController
@RequestMapping("/card")
public class SmartCardController {

	@Autowired
	private SmartCardService smartCardService;
	
	@PostMapping(value="/deactivate")
	public ResponseEntity<CommonResponse> deactivateSmartCard(Long cardId, String reason){
		
		CommonResponse response = smartCardService.deactivateSmartCard(cardId, reason);
		
		return ResponseEntity
                .status(Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())))
                .body(response);
	}
	
	@PostMapping(value="/check/balance")
	public ResponseEntity<CommonResponse> balanceCheck(Long cardId){
		CommonResponse response = smartCardService.checkBalance(cardId);

		return ResponseEntity.status(Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())))
				.body(response);
	}
}
