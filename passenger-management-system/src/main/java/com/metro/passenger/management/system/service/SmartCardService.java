package com.metro.passenger.management.system.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.metro.passenger.management.system.model.Passenger;
import com.metro.passenger.management.system.model.SmartCard;
import com.metro.passenger.management.system.reponse.CardBalance;
import com.metro.passenger.management.system.reponse.CommonResponse;
import com.metro.passenger.management.system.repository.SmartCardRepository;
import com.metro.passenger.management.system.util.SmartCardStatus;

@Component
public class SmartCardService {

	@Autowired
	private SmartCardRepository cardRepository;
	
	public CommonResponse deactivateSmartCard(Long cardId, String reason){
		CommonResponse response = new CommonResponse();
		
		Optional<SmartCard> cardOpt = cardRepository.findById(cardId);
		
		if(cardOpt.isPresent() && SmartCardStatus.ACTIVE.equals(cardOpt.get().getStatus())){
			SmartCard card = cardOpt.get();
			card.setStatus(SmartCardStatus.INACTIVE);
			card.setDeactivatedOn(new Date());
			card.setReason(reason);
			
			cardRepository.save(card);
			response.setReturnMessage("Card deactivated successfully");
			response.setStatusCode(HttpStatus.OK.value());
		}else{
			response.setReturnMessage("Card is not available");
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
		}
		
		//After deactivation send to user to remove the current that card data and move to history by putting this to kafka
		return response;
	}
	
	public CommonResponse createSmartCard(Passenger passenger){
		CommonResponse response = new CommonResponse();

		SmartCard card = new SmartCard();
		card.setCreatedOn(new Date());
		card.setStatus(SmartCardStatus.ACTIVE);
		card.setUserId(passenger.getUserId());
		card.setBalance(0.0);
		cardRepository.save(card);

		response.setStatusCode(HttpStatus.OK.value());
		response.setReturnMessage("Card created successfully");
		return response;
	}
	
	public CardBalance checkBalance(Long cardId) {
		Optional<SmartCard> smartCardOpt = cardRepository.findById(cardId);
		
		CardBalance response = new CardBalance();
		if(smartCardOpt.isPresent() && SmartCardStatus.ACTIVE.equals(smartCardOpt.get().getStatus())){
			SmartCard smartCard = smartCardOpt.get();
			response.setBalance(smartCard.getBalance());
			response.setStatusCode(HttpStatus.OK.value());
			response.setReturnMessage("Card balance is Rs " + smartCard.getBalance());
		}else{
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setReturnMessage("Card is not available");
		}
		
		return response;

	}
	
	public CommonResponse deductBalance(Long cardId, double fareAmount){
		Optional<SmartCard> smartCardOpt = cardRepository.findById(cardId);
		
		if(smartCardOpt.isPresent()){
			SmartCard smartCard = smartCardOpt.get();
			smartCard.setBalance(smartCard.getBalance()-fareAmount);
			
			cardRepository.save(smartCard);
		}
		return new CommonResponse(HttpStatus.OK.value(), "Success");
	}
}
