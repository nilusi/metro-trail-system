package com.metro.passenger.management.system.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.metro.passenger.management.system.dto.Trip;
import com.metro.passenger.management.system.service.PaymentService;
import com.metro.passenger.management.system.util.CommonConverter;

@Component
public class Consumer {
    public static final String KAFKA_TOPIC = "trip";
    public static final String GROUP_ID = "kafka-sandbox";

    @Autowired
    CommonConverter commonConverter;

	@Autowired
	PaymentService paymentService;

    @KafkaListener(
            topics = KAFKA_TOPIC,
            groupId = GROUP_ID
    )
    public void listen(String message) {
        System.out.println("Received  via kafka listener.."+message);
        Trip trip = commonConverter.jsonToObject(message, Trip.class);
        paymentService.addTransaction(trip);
    }
}
