package com.metro.passenger.management.system.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metro.passenger.management.system.exception.InvalidRequestException;
import com.metro.passenger.management.system.reponse.CommonResponse;
import com.metro.passenger.management.system.request.SmartCardApplyRequest;
import com.metro.passenger.management.system.service.PassengerService;

@RestController
@RequestMapping("/passenger")
public class PassengerController {

	@Autowired
	private PassengerService passengerService;
	
	@PostMapping(value="/apply")
	public ResponseEntity<CommonResponse> applyForSmartCard(@RequestBody SmartCardApplyRequest request){
		CommonResponse response =passengerService.applyForSmartCard(request);
		
		return ResponseEntity
                .status(Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())))
                .body(response);
	}
	
	@PostMapping(value = "/checkin")
	public ResponseEntity<CommonResponse> checkIn(Long cardId) {
		CommonResponse response;
		try {
			response = passengerService.checkIn(cardId);
		} catch (InvalidRequestException e) {
			response = new CommonResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage());
		}
		return ResponseEntity.status(Objects.requireNonNull(HttpStatus.resolve(response.getStatusCode())))
				.body(response);
	}

}
