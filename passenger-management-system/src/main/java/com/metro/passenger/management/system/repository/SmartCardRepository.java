package com.metro.passenger.management.system.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.metro.passenger.management.system.model.SmartCard;
import com.metro.passenger.management.system.util.SmartCardStatus;

@Repository
public interface SmartCardRepository extends CrudRepository<SmartCard, Long> {

	Optional<SmartCard> findByCardIdAndStatus(Long cardId, SmartCardStatus active);

}
